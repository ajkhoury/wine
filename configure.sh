#!/bin/bash

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
WINE_ROOT="$DIR"
BUILD_DIR="$WINE_ROOT/../build"

CONFLAGS="--with-gnutls --without-oss --without-sane --without-netapi"

mkdir -p ${BUILD_DIR}/wine64 && mkdir -p ${BUILD_DIR}/wine32

cd ${BUILD_DIR}/wine64 && ${WINE_ROOT}/configure CFLAGS="-Og -ggdb3" CROSSCFLAGS="-Og -ggdb3" \
--prefix=/opt/wine-devel \
--libdir=/opt/wine-devel/lib64 \
--mandir=/opt/wine-devel/share/man \
--infodir=/opt/wine-devel/share/info \
${CONFLAGS} --enable-win64

if [ $? -ne 0 ]; then exit 1; fi

cd ${BUILD_DIR}/wine32 && PKG_CONFIG_PATH=/usr/lib/i386-linux-gnu/pkgconfig ${WINE_ROOT}/configure CFLAGS="-Og -ggdb3" CROSSCFLAGS="-Og -ggdb3" \
--prefix=/opt/wine-devel \
--libdir=/opt/wine-devel/lib \
--mandir=/opt/wine-devel/share/man \
--infodir=/opt/wine-devel/share/info \
${CONFLAGS} --with-wine64=${BUILD_DIR}/wine64

if [ $? -ne 0 ]; then exit 1; fi
